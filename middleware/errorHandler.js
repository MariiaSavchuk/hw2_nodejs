function errorHandler (error, req, res, next) {
    console.error(`error: ${error.name}, ${error.message}`)
    res.status(500).send({'message': error.message});
}

module.exports = errorHandler;