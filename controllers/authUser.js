const User = require('../models/Users.sjs');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

async function registerUser(req, res) {
    const {username, password} = req.body;
    const userExists = await User.findOne({username: username});
    if (userExists) {
        return res.status(400).send('User with such name already exists!')
    }
    const hashedPass = await bcrypt.hash(password, 10);

    const user = new User({
        username: username,
        password: hashedPass
    })

    try {
        const newUser = await user.save();
        res.status(200).json({"message": "Success"})
    } catch (error) {
        res.status(400).json({"message": error.message});
    }
}

async function loginUser(req, res) {
    const foundUser = await User.findOne({username: req.body.username});
    if (!foundUser) {
        return res.status(400).json({"message":'Doesn`t exists!'})
    }

    const match = await bcrypt.compare(req.body.password, foundUser.password);
    if (!match) {
        return res.status(403).send('Access denied')
    }

    const token = jwt.sign({
        _id: foundUser._id},
        process.env.ACCESS_TOKEN_SECRET
        )

    res.json({message: "success", jwt_token: token})
}

module.exports = {
    registerUser,
    loginUser
}