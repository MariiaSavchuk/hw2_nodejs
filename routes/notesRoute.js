const express = require('express');
const router = express.Router();
const verifyJWT = require('../middleware/verifyJWT');
const {
    getNotes, 
    postNote, 
    getNoteById, 
    updateNoteById, 
    toggleNoteCheck, 
    deleteNoteById
} = require('../controllers/handleNotes');

router.get('/', verifyJWT, getNotes)
router.post('/', verifyJWT, postNote);
router.get('/:id', verifyJWT, getNoteById);
router.delete('/:id', verifyJWT, deleteNoteById);
router.put('/:id', verifyJWT, updateNoteById)
router.patch('/:id', verifyJWT, toggleNoteCheck)

module.exports = router;