const express = require('express');
const router = express.Router();
const verifyJWT = require('../middleware/verifyJWT');

const {
    getUser, 
    deleteUser, 
    updateUser
} = require('../controllers/handleUser')

router.get('/me', verifyJWT, getUser);

router.delete('/me', verifyJWT, deleteUser)

router.patch('/me', verifyJWT, updateUser)

module.exports = router;